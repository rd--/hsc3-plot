# plot

## buffer

Plot Sc3 buffer.

## csv

Plot rows or columns from a Csv file, cells are read as floating point data.

Plot data from Csv files.  Formats are _line_ or _discrete_ or _scatter_.
Column numbers to plot for each axis are given as zero-indexed integers.

## sf

Plot sound file.
