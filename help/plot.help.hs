import Sound.Sc3.Plot {- hsc3-plot -}

-- > plot_p2 (attr_frac "lines" 0.5) p2_01
p2_01 :: [[(Double, Double)]]
p2_01 =
  let x = [-pi,-pi + 0.01 .. pi]
      f r t = (r t * cos t,r t * sin t)
  in [zip (map cos x) (map sin x)
     ,zip (map cos x) (map (sin . (* 3)) x)
     ,map (f ((/ (2 * pi)) . (+ pi))) x]

-- > plot_p3 (const "with lines") p3_01
p3_01 :: [[(Double, Double, Double)]]
p3_01 =
  let t' = [-pi,-pi + 0.01 .. pi]
      f0 n d = sin . (+) d . (*) n
      f1 t = (f0 1 (pi/2) t,f0 3 0 t,f0 5 0 t)
      e' = [0,0.005 .. pi]
      f2 a b r e t = ((a * t + r * sin e) * cos t
                     ,(a * t + r * sin e) * sin t
                     ,b * t + r * (1 - cos e))
  in [map f1 t',zipWith (f2 0.25 0.25 0.25) e' t']

-- > plotMatrix ["set palette color"] mx_01
mx_01 :: [[Double]]
mx_01 =
  [[1.00000000,1.00000000,1.00000000,0.73961496]
  ,[1.00000000,1.00000000,1.00000000,0.39490538]
  ,[0.53443549,0.31331112,0.90917979,0.58216201]
  ,[0.35888692,0.7361968,0.95389629,0.94283073]
  ,[0.85763543,0.1405479,0.78166569,0.43739318]
  ,[0.18519824,0.31907815,0.18394244,0.01633875]
  ,[0.0442339,0.33393132,0.77247883,0.79683943]
  ,[0.8472137,0.42471225,0.94257581,0.70417117]]

-- > plot_vectors vc_01
vc_01 :: [[((Int,Int),(Int,Int))]]
vc_01 =
  let p1 = [((0,0),(2,1)),((2,2),(3,2)),((3,4),(4,1))]
      p2 = let d_ = [1,2,3,2,3,2,1]
               x_ = 0 : scanl1 (+) d_
               y_ = [6,4,5,3,7,2,8]
               f x y d = ((x,y),(x+d,y))
               in zipWith3 f x_ y_ d_
  in [p1,p2]
