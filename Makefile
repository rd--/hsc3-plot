all:
	echo "hsc3-plot"

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd; make clean)

mk-cmd:
	(cd cmd ; make all install)

push-all:
	r.gitlab-push.sh hsc3-plot

push-tags:
	r.gitlab-push.sh hsc3-plot --tags

indent:
	fourmolu -i Sound

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
