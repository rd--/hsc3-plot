import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.List.Split as Split {- split -}
import qualified Text.CSV.Lazy.String as Csv.Lazy {- lazy-csv -}

import qualified Music.Theory.Array.Csv as Csv {- hmt-base -}
import qualified Music.Theory.Array.Json as Json {- hmt-base -}
import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Plot as Plot {- hsc3-plot -}
import qualified Sound.Sc3.Plot.Displace as Plot.Displace {- hsc3-plot -}

-- * Buffer

plot_buffer :: Bool -> Int -> IO ()
plot_buffer wt ix = do
  let plot_f = if wt then Plot.plot_wavetable else Plot.plot_buffer
  Sc3.withSc3 (plot_f ix)

-- * Csv X

p1_plot_fmt :: String -> [Plot.Table Double] -> IO ()
p1_plot_fmt fmt =
  case fmt of
    "line" -> Plot.plot_p1_ln
    "discrete" -> Plot.plot_p1_imp
    "scatter" -> Plot.plot_p1_pt
    _ -> error ("p1_plot_fmt: unknown format: " ++ fmt)

plot_csv :: Bool -> String -> FilePath -> [Int] -> IO ()
plot_csv rows fmt fn ix_set = do
  let prs = fromMaybe 0.0 . Sc3.parse_double
  tbl <- fmap (if rows then id else transpose) (Csv.csv_table_read_def prs fn)
  let sel = map ((!!) tbl) ix_set
  (p1_plot_fmt fmt) sel

-- * Csv Xy & Xyz

p2_plot_fmt :: String -> [Plot.Coord Double] -> IO ()
p2_plot_fmt fmt =
  case fmt of
    "line" -> Plot.plot_p2_ln
    "scatter" -> Plot.plot_p2_pt
    _ -> error ("p2_plot_fmt: unknown format: " ++ fmt)

csv_load :: (String -> n) -> FilePath -> IO [[n]]
csv_load f fn = do
  s <- readFile fn
  let t = Csv.Lazy.fromCSVTable (Csv.Lazy.csvTable (Csv.Lazy.parseCSV s))
  return (map (map f) t)

csv_load_double :: FilePath -> IO [[Double]]
csv_load_double = csv_load read

csv_load_col :: FilePath -> IO [[Double]]
csv_load_col = fmap transpose . csv_load_double

with_csv_col :: ([[Double]] -> IO r) -> FilePath -> IO r
with_csv_col f fn = csv_load_col fn >>= \c -> f c

csv_plot_xy :: (Int,[Int]) -> FilePath -> IO ()
csv_plot_xy (i,j_seq) = with_csv_col (\c -> Plot.plot_p2_ln (map (\j -> zip (c !! i) (c !! j)) j_seq))

csv_plot_3 :: ([[(Double,Double,Double)]] -> IO ()) -> (Int,Int,Int) -> FilePath -> IO ()
csv_plot_3 f (i,j,k) = with_csv_col (\c -> f [zip3 (c !! i) (c !! j) (c !! k)])

-- * Json X

plot_json :: Bool -> String -> FilePath -> [Int] -> IO ()
plot_json rows fmt fn ix_set = do
  mtx <- json_load_double fn
  let tbl = if rows then mtx else transpose mtx
      sel = map ((!!) tbl) ix_set
  p1_plot_fmt fmt sel

-- * Json Xy & Xyz

json_load_double :: FilePath -> IO [[Double]]
json_load_double fn = do
  s <- readFile fn
  return (Json.decodeMatrixOfDouble s)

json_load_col :: FilePath -> IO [[Double]]
json_load_col = fmap transpose . json_load_double

with_json_row :: ([[Double]] -> IO r) -> FilePath -> IO r
with_json_row f fn = json_load_double fn >>= \c -> f c

with_json_col :: ([[Double]] -> IO r) -> FilePath -> IO r
with_json_col f fn = json_load_col fn >>= \c -> f c

json_plot_xy :: String -> (Int,[Int]) -> FilePath -> IO ()
json_plot_xy fmt (i,j_seq) = with_json_col (\c -> (p2_plot_fmt fmt) (map (\j -> zip (c !! i) (c !! j)) j_seq))

json_plot_3 :: ([[(Double,Double,Double)]] -> IO ()) -> (Int,Int,Int) -> FilePath -> IO ()
json_plot_3 f (i,j,k) = with_json_col (\c -> f [zip3 (c !! i) (c !! j) (c !! k)])

json_plot_matrix :: String -> FilePath -> IO ()
json_plot_matrix clr = with_json_row (\c -> Plot.plotMatrix ["set palette " ++ clr] c)

-- * Typ

typ_plot_matrix :: String -> String -> FilePath -> IO ()
typ_plot_matrix typ clr fn =
  case typ of
    "json" -> json_plot_matrix clr fn
    _ -> error "typ_plot_matrix?"

typ_plot_2 :: String -> Bool -> String -> FilePath -> [Int] -> IO ()
typ_plot_2 typ rows fmt fn ix_set =
  case typ of
    "csv" -> plot_csv rows fmt fn ix_set
    "json" -> plot_json rows fmt fn ix_set
    _ -> error "typ_plot_2?"

typ_plot_xy :: String -> String -> (Int,[Int]) -> FilePath -> IO ()
typ_plot_xy typ fmt opt fn =
  case typ of
    "csv" -> csv_plot_xy opt fn
    "json" -> json_plot_xy fmt opt fn
    _ -> error "typ_plot_xy?"


typ_plot_3 :: String -> ([[(Double,Double,Double)]] -> IO ()) -> (Int,Int,Int) -> FilePath -> IO ()
typ_plot_3 typ f ijk fn =
  case typ of
    "csv" -> csv_plot_3 f ijk fn
    "json" -> json_plot_3 f ijk fn
    _ -> error "typ_plot_3?"

-- * ScSynDef

scsyndef_plot :: FilePath -> Int -> Double -> IO ()
scsyndef_plot syndefFilename nc dur = do
  bytes <- ByteString.readFile syndefFilename
  Plot.plot_encoded_graphdef_nrt (48000,48) nc dur bytes

-- * Sf

plot_sf :: Bool -> Maybe [Int] -> Maybe (Int, Int) -> [FilePath] -> IO ()
plot_sf dspl ch rng fn = do
  let plot_f = if dspl then Plot.Displace.plot_sf_set_displace else Plot.plot_sf_set
  plot_f ch rng fn

-- * Main

opt_def :: [Opt.OptUsr]
opt_def =
  [("displace","False","Bool","draw channels separately")
  ,("channels","","[Int]","zero-indexed channels indices (comma separated)")
  ,("format","line","String","line, discrete or scatter plot")
  ,("palette","","String","set colour palette")
  ,("range","nil","Int","zero-indexed left and right frame indices (comma separated)")
  ,("rows","False","Bool","plot rows instead of columns")
  ,("wavetable","False","Bool","buffer is in wavetable format")]

usage :: [String]
usage =
    ["hsc3-plot cmd [arg...]"
    ," buffer [--wavetable] ix"
    ," csv|json matrix [--palette] file-name"
    ," csv|json x [--rows --format] file-name x..."
    ," csv|json xy [--format] file-name x y..."
    ," csv|json xyz [--format] file-name x y z"
    ," scsyndef file-name number-of-channels duration:seconds"
    ," sf [--channels --displace -range] file-name..."
    ,"   format=line,discrete,scatter"
    ]

{- | Parse Int list

>>> parse_int_list "1,2"
[1,2]
-}
parse_int_list :: String -> [Int]
parse_int_list x = read ("[" ++ x ++ "]")

{- | Parse Int range

>>> parse_range "0,1024"
(0,1024)
-}
parse_range :: String -> (Int,Int)
parse_range x =
    case Split.splitOn "," x of
      [l,r] -> (read l,read r)
      _ -> error "parse_range"

main :: IO ()
main = do
  (o,a) <- Opt.opt_get_arg True usage opt_def
  case a of
    ["buffer",ix] -> plot_buffer (Opt.opt_read o "wavetable") (read ix)
    [typ,"matrix",fn] -> typ_plot_matrix typ (Opt.opt_get o "palette") fn
    typ:"x":fn:ix ->
      typ_plot_2
      typ
      (Opt.opt_read o "rows")
      (Opt.opt_get o "format")
      fn
      (map read ix)
    typ:"xy":fn:x:y -> typ_plot_xy typ (Opt.opt_get o "format") (read x,map read y) fn
    [typ,"xyz",fn,x,y,z] ->
      typ_plot_3
      typ
      (case Opt.opt_get o "format" of
          "line" -> Plot.plot_p3_ln
          "discrete" -> Plot.plot_p3_imp
          "scatter" -> Plot.plot_p3_pt
          _ -> error "plot: unknown format")
      (read x,read y,read z)
      fn
    ["scsyndef",fn,nc,dur] -> scsyndef_plot fn (read nc) (read dur)
    "sf":fn_seq ->
      plot_sf
      (Opt.opt_read o "displace")
      (fmap parse_int_list (Opt.opt_get_nil o "channels"))
      (fmap parse_range (Opt.opt_get_nil o "range"))
      fn_seq
    _ -> Opt.opt_usage usage opt_def
