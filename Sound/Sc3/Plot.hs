-- | Simple plot functions for "Sound.Sc3".
module Sound.Sc3.Plot where

import Control.Monad {- base -}
import Data.Char {- base -}
import Data.List {- base -}
import Data.List.Split {- split -}
import Data.Ratio {- base -}
import System.Directory {- directory -}
import System.FilePath {- filepath -}
import System.Process {- process -}
import Text.Printf {- base -}

import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Text.CSV.Lazy.String as Csv {- lazy-csv -}

import qualified Music.Theory.List as List {- hmt-base -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Common.Buffer.Vector as Sc3.Vector {- hsc3 -}
import qualified Sound.Sc3.Server.Nrt.Ugen as Nrt {- hsc3 -}

import qualified Sound.File.HSndFile as Sf.SndFile {- hsc3-sf -}

-- * Math

-- | 'minimum' and 'maximum'.
minmax :: Ord t => [t] -> (t, t)
minmax l = (minimum l, maximum l)

-- | 'linlin_hs' of /z/.
normalise_seq :: Fractional b => (b, b) -> (b, b) -> [b] -> [b]
normalise_seq d r = map (linlin_hs d r)

-- | Variant that derives domain from 'minmax' of /z/.
normalise_seq' :: (Fractional b, Ord b) => (b, b) -> [b] -> [b]
normalise_seq' (l, r) z = normalise_seq (minmax z) (l, r) z

-- | /n/ '*' /n/.
square :: Num a => a -> a
square n = n * n

-- | Root mean square.
rms :: Floating a => [a] -> a
rms l =
  let n = fromIntegral (length l)
      s = sum (map square l)
  in sqrt (s / n)

-- | 'rms' of 'chunksOf'.
resample_rms :: Floating b => Int -> [b] -> [b]
resample_rms m l =
  let n = length l
      c = n `div` m
      s = chunksOf c l
  in map rms s

-- * Data

{- | Class of plottable numbers.
This is necessary to allow 'Rational', the show instance of which cannot be read by @gnuplot@.
-}
class (Num n, Vector.Storable n) => PNum n where
  pshow :: n -> String

ratio_to_double :: Integral n => Ratio n -> Double
ratio_to_double = realToFrac

instance PNum Int where pshow = show

{- instance PNum Integer where pshow = show -}
instance PNum Float where pshow = show
instance PNum Double where pshow = show
instance (Integral n, Vector.Storable n) => PNum (Ratio n) where pshow = show . ratio_to_double

-- * Attr

{- | Attributes are generated given (i,j) where /i/ is the data set
(one indexed) and /j/ is the number of data sets.
-}
type Attr = (Int, Int) -> String

{- | Attribute with fractional value.

>>> attr_frac "lines" 0.5 (1,2)
"with lines lt palette frac 0.25"
-}
attr_frac :: String -> Double -> Attr
attr_frac ty r (i, j) =
  let n = fromIntegral (j - i) / fromIntegral j
      n' = linlin_hs (0, 1) (0, r) n
  in "with " ++ ty ++ " lt palette frac " ++ show n'

-- * 1-dimensional

-- | List of /y/ values, at (implicit) equal /x/ increments.
type Table t = [t]

-- | Plot set of 'Table', given 'Attr'.
plot_p1 :: PNum t => Attr -> [Table t] -> IO ()
plot_p1 attr =
  mkPlot (["set palette gray", "unset colorbox"], "plot", attr)
    . map (map return)

{- | 'plot_p1' as line segments.

> plot_p1_ln [[0,2..12],[0..6],[0,4..12]]
> plot_p1_ln [map (\x -> x * cos (x / 20)) [-400 .. 800]]
-}
plot_p1_ln :: PNum t => [Table t] -> IO ()
plot_p1_ln = plot_p1 (attr_frac "lines" 0.5)

{- | 'plot_p1' as points (scatter plot)

> plot_p1_pt [[1 .. 100],[1,1.5 .. 50]]
-}
plot_p1_pt :: PNum t => [Table t] -> IO ()
plot_p1_pt = plot_p1 (attr_frac "points pt 0" 0.5)

{- | 'plot_p1' as steps.

> plot_p1_stp [[0,2..12],[0..6],[0,4..12]]
-}
plot_p1_stp :: PNum t => [Table t] -> IO ()
plot_p1_stp = plot_p1 (attr_frac "steps" 0.5)

{- | 'plot_p1' as impulses (discrete or pin or stem plot).

> plot_p1_imp [[0,2..12],[0..6],[0,4..12]]
> plot_p1_imp [map (\x -> x * cos (x / 20)) [-400 .. 800]]
-}
plot_p1_imp :: PNum t => [[t]] -> IO ()
plot_p1_imp = plot_p1 (attr_frac "impulses" 0.5)

-- * 2-dimensional

-- | Cartesian (/x/,/y/) pair.
type P2 t = (t, t)

-- | List of 'P2'.
type Coord t = [P2 t]

-- | (Param,Data)
type Plot_Dat t = (Plot_Param, [[[t]]])

plot_p2_dat :: Attr -> [Coord t] -> Plot_Dat t
plot_p2_dat attr coord =
  let f (x, y) = [x, y]
  in ((["set palette gray", "unset colorbox"], "plot", attr), map (map f) coord)

-- | Plot set of 'Coord'.
plot_p2 :: PNum t => Attr -> [Coord t] -> IO ()
plot_p2 attr = mk_plot_def . plot_p2_dat attr

plot_p2_ln_dat :: [Coord t] -> Plot_Dat t
plot_p2_ln_dat = plot_p2_dat (attr_frac "lines" 0.5)

{- | 'plot_p2' of @with lines@.

> let x = [-pi,-pi + 0.01 .. pi]
> plot_p2_ln [zip (map cos x) (map sin x)]
-}
plot_p2_ln :: PNum t => [Coord t] -> IO ()
plot_p2_ln = mk_plot_def . plot_p2_ln_dat

{- | 'plot_p2' of @with points@.

> let f a b c d (x,y) = (x ** 2 - y ** 2 + a * x + b * y,2 * x * y + c * x + d * y)
> plot_p2_pt [take 5000 (iterate (f 0.9 (-0.6013) 2.0 0.5) (0.1,0.0))]
-}
plot_p2_pt :: PNum t => [Coord t] -> IO ()
plot_p2_pt = plot_p2 (attr_frac "points pt 0" 0.5)

{- | 'plot_p2' of @with steps@.

> plot_p2_stp [[(0,0),(1,1),(2,0.5),(4,6),(5,1),(6,0.25)]]
-}
plot_p2_stp :: PNum t => [Coord t] -> IO ()
plot_p2_stp = plot_p2 (attr_frac "steps" 0.5)

-- * 3-dimensional

-- | Cartesian (/x/,/y/,/z/) triple.
type P3 t = (t, t, t)

-- | List of 'P3'.
type Path t = [P3 t]

-- | Three-dimensional variant of 'plot_p2'.
plot_p3 :: PNum t => Attr -> [Path t] -> IO ()
plot_p3 attr p = do
  let f (x, y, z) = [x, y, z]
  mkPlot ([], "splot", attr) (map (map f) p)

-- | 'plot_p3' of @with lines@.
plot_p3_ln :: PNum t => [Path t] -> IO ()
plot_p3_ln = plot_p3 (const "with lines")

-- | 'plot_p3' of @with points pt 0@.
plot_p3_pt :: PNum t => [Path t] -> IO ()
plot_p3_pt = plot_p3 (const "with points pt 0")

-- | 'plot_p3' of @with impulses@.
plot_p3_imp :: PNum t => [Path t] -> IO ()
plot_p3_imp = plot_p3 (const "with impulses")

-- * Vector (Cartesian)

-- | List of ('P2','P2') vectors.
type Vc t = [(P2 t, P2 t)]

plot_vectors_with_style :: PNum t => String -> [Vc t] -> IO ()
plot_vectors_with_style style = do
  let f ((x0, y0), (x1, y1)) = [x0, y0, x1 - x0, y1 - y0]
  mkPlot ([], "plot", const ("with vectors " ++ style)) . map (map f)

{- | Plot vectors given as (/p/,/q/).

> plot_vectors [[((0,0),(2,1)),((2,2),(3,2)),((3,4),(4,1))]]
-}
plot_vectors :: PNum t => [Vc t] -> IO ()
plot_vectors = plot_vectors_with_style "linewidth 1.25"

{- | Plot lines given as (/p/,/q/).

> plot_lines [[((0,0),(2,1)),((2,2),(3,2)),((3,4),(4,1))]]
-}
plot_lines :: PNum t => [Vc t] -> IO ()
plot_lines = plot_vectors_with_style "nohead linewidth 1.25"

-- * Matrix

{- | Plot regular matrix data.

> plotMatrix ["set palette grey","unset colorbox"] [[1,3,2],[6,4,5],[8,9,7]]
-}
plotMatrix :: PNum t => [String] -> [[t]] -> IO ()
plotMatrix opt =
  mkPlot (opt, "plot", const "matrix with image")
    . return

-- * Envelope

{- | Plot 'Envelope' data.

> import Sound.Sc3
> plotEnvelope [envPerc 0.2 1,envSine 1 0.75,envAdsr 0.4 0.4 0.8 0.9 1 (EnvNum (-4)) 0]
-}
plotEnvelope :: (PNum t, Ord t, Floating t, Enum t) => [Envelope t] -> IO ()
plotEnvelope = plot_p2_ln . map (envelope_render 256)

-- * Buffer

plot_buffer :: (MonadIO m, DuplexOsc m) => Int -> m ()
plot_buffer k = b_fetch 512 k >>= liftIO . plot_p1_ln

plot_wavetable :: (DuplexOsc m, MonadIO m) => Int -> m ()
plot_wavetable k = b_fetch 512 k >>= liftIO . plot_p1_ln . map from_wavetable

{- | 'plot_p1_ln' of 'b_fetch1'.

> withSc3 (plot_buffer1 12)
-}
plot_buffer1 :: (MonadIO m, DuplexOsc m) => Int -> m ()
plot_buffer1 k = b_fetch1 512 k >>= liftIO . plot_p1_ln . return

-- | 'plot_p1_ln' of 'C.from_wavetable' of 'b_fetch1'.
plot_wavetable1 :: (DuplexOsc m, MonadIO m) => Int -> m ()
plot_wavetable1 k = b_fetch1 512 k >>= liftIO . plot_p1_ln . return . from_wavetable

{- | ''plot_p1_ln' of 'resample_rms' of 'b_fetch1'.

> withSc3 (plot_buffer1_rms 512 0)
-}
plot_buffer1_rms :: (MonadIO m, DuplexOsc m) => Int -> Int -> m ()
plot_buffer1_rms n k =
  b_fetch1 512 k
    >>= liftIO . plot_p1_ln . return . (zipWith (*) (cycle [1, -1])) . resample_rms n

{- | 'plot_table1_resamp1' of 'b_fetch1'.

> withSc3 (plot_buffer1_resamp1 512 0)
-}
plot_buffer1_resamp1 :: (MonadIO m, DuplexOsc m) => Int -> Int -> m ()
plot_buffer1_resamp1 n k = b_fetch1 512 k >>= liftIO . plot_table1_resamp1 n . Vector.fromList

-- * Vector (Data Type)

-- | 'plot_p1_ln' of 'Vector.toList'.
plot_table1_vector :: PNum t => Vector.Vector t -> IO ()
plot_table1_vector = plot_p1_ln . return . Vector.toList

{- | 'plot_table1_vector' of 'resamp1'.

> d <- withSc3 (b_fetch1 512 0)
> plot_table1_resamp1 1024 (Vector.fromList d)
-}
plot_table1_resamp1 :: (RealFrac t, PNum t) => Int -> Vector.Vector t -> IO ()
plot_table1_resamp1 n = plot_table1_vector . Sc3.Vector.resamp1 n

-- * Soundfile

{- | Interior range of list, zero indexed.  Ie. 'drop' until left and 'take' one more than the difference.

>>> l_range (1,3) "abcd"
"bcd"
-}
l_range :: (Int, Int) -> [a] -> [a]
l_range (l, r) = take (r - l + 1) . drop l

load_sf_ch_rng :: Maybe [Int] -> Maybe (Int, Int) -> FilePath -> IO [[Double]]
load_sf_ch_rng ix k nm = do
  (h, c) <- Sf.SndFile.read nm
  when (maybe 0 maximum ix >= Sf.SndFile.channelCount h) (error "load_sf_ch_rng: channel?")
  when (maybe 0 fst k < 0 || maybe 0 snd k > Sf.SndFile.frameCount h) (error "plot_sf_ch_rng: frame?")
  let c' = maybe c (map (c !!)) ix
      f ch = maybe ch (flip l_range ch) k
  return (map f c')

{- | Plot first channel of a sound file.
If a sample range is given, plot only that range.

> let fn = "/home/rohan/data/audio/pf-c5.au"
> plot_sf_ch_rng plot_p1_ln Nothing (Just (0,511)) fn

> let fn = "/home/rohan/data/audio/pf-c5.wav"
> plot_sf_ch_rng plot_p1_ln (Just [0,1]) (Just (2048,4095)) fn
-}
plot_sf_ch_rng :: ([[Double]] -> IO ()) -> Maybe [Int] -> Maybe (Int, Int) -> FilePath -> IO ()
plot_sf_ch_rng plot_f ix k nm = load_sf_ch_rng ix k nm >>= plot_f

{- | Plot first channel of a sound file.
If a sample range is given, plot only that range.

> let fn = "/home/rohan/data/audio/pf-c5.au"
> plot_sf1 (Just (2048,4095)) fn
-}
plot_sf1 :: Maybe (Int, Int) -> FilePath -> IO ()
plot_sf1 = plot_sf_ch_rng plot_p1_ln (Just [0])

plot_sf :: FilePath -> IO ()
plot_sf = plot_sf_ch_rng plot_p1_ln Nothing Nothing

-- * Sf-Set

plot_sf_set_f :: ([[Double]] -> IO ()) -> Maybe [Int] -> Maybe (Int, Int) -> [FilePath] -> IO ()
plot_sf_set_f plot_f ix k nm = mapM (load_sf_ch_rng ix k) nm >>= plot_f . concat

plot_sf_set :: Maybe [Int] -> Maybe (Int, Int) -> [FilePath] -> IO ()
plot_sf_set ix k = plot_sf_set_f plot_p1_ln ix k

-- * Ugen

type Plot_Buffer_F = Int -> Connection OscSocket ()

-- | (Buffer, Sample-rate, Number-of-channels, Plot-function)
type Plot_Ugen_Opt = (Int, Double, Maybe Int, Plot_Buffer_F)

{- | Plot /dur/ seconds of n-ary Ugen audio or control rate graph via Nrt.

> plot_ugen_nrt (48000,64) 1.0 (sinOsc ar (mce2 2 3) 0 * 0.1)
> plot_ugen_nrt (400,1) 1.0 (sinOsc kr (mce2 2 3) 0 * 0.1)
-}
plot_ugen_nrt :: (Int, Int) -> Time -> Ugen -> IO ()
plot_ugen_nrt (sample_rate, block_size) dur u = do
  let opt = ("/tmp/u.osc", "/tmp/u.wav", sample_rate, PcmFloat, ["-z", show block_size])
  Nrt.nrt_ugen_render opt dur u
  plot_sf "/tmp/u.wav"

plot_encoded_graphdef_nrt :: (Int, Int) -> Int -> Time -> Blob -> IO ()
plot_encoded_graphdef_nrt (sample_rate, block_size) nc dur bytes = do
  let opt = ("/tmp/u.osc", "/tmp/u.wav", sample_rate, PcmFloat, ["-z", show block_size])
  Nrt.nrt_encoded_graphdef_render opt dur nc bytes
  plot_sf "/tmp/u.wav"

{- | Plot /dur/ seconds of n-ary Ugen graph with given options.

> let f = xLine kr 1 800 0.1 DoNothing
> let opt = (1000,48000,Nothing,Sound.Sc3.Plot.Displace.plot_buffer_displace)
> plot_ugen_opt opt 0.1 (mce [lfTri ar f 0,lfSaw ar f 0])
-}
plot_ugen_opt :: Plot_Ugen_Opt -> Time -> Ugen -> IO ()
plot_ugen_opt (b, sr, nc_m, plot_f) dur u = do
  let nc = length (mceChannels u)
      r = recordBuf AudioRate (constant b) 0 1 0 1 NoLoop 1 RemoveSynth u
      act = do
        _ <- async (b_alloc b (floor (sr * dur)) nc)
        play (mrg2 (out 0 (silent 1)) r)
        pauseThread dur
        plot_f b
  when (maybe False (/= nc) nc_m) (error "plot_ugen: channel mismatch")
  withSc3 act

{- | Plot /n/ seconds of one channel Ugen graph.

> plot_ugen1 0.1 (lfTri ar (xLine kr 1 800 0.1 DoNothing) 0)
-}
plot_ugen1 :: Double -> Ugen -> IO ()
plot_ugen1 = plot_ugen_opt (1000, 48000, Just 1, plot_buffer)

{- | Plot /n/ seconds of n-ary channel Ugen graph.

> let f = xLine kr 1 800 0.1 DoNothing
> plot_ugen 0.1 (mce [lfTri ar f 0,lfSaw ar f 0])
-}
plot_ugen :: Double -> Ugen -> IO ()
plot_ugen = plot_ugen_opt (1000, 48000, Nothing, plot_buffer)

-- * Function

plot_fn_r1_ln_x :: PNum t => [t -> t] -> [t] -> IO ()
plot_fn_r1_ln_x l x = plot_p2_ln (map (\f -> zip x (map f x)) l)

plot_rng_to_x_incl :: (Enum n, Fractional n) => n -> (n, n) -> [n]
plot_rng_to_x_incl n (lhs, rhs) = let i = (rhs - lhs) / n in [lhs, lhs + i .. rhs]

plot_rng_to_x_excl :: (Enum n, Fractional n) => n -> (n, n) -> [n]
plot_rng_to_x_excl n (lhs, rhs) = let i = (rhs - lhs) / n in [lhs + i, lhs + (i * 2) .. rhs - i]

{- | Plot the real function /f/ over the inclusive range (lhs,rhs),
at 1000 equally distributed points.

> plot_fn_r1_ln [sin] (-pi,pi)
-}
plot_fn_r1_ln :: [Double -> Double] -> (Double, Double) -> IO ()
plot_fn_r1_ln f = plot_fn_r1_ln_x f . plot_rng_to_x_incl 1000

-- | Variant where range is exclusive.
plot_fn_r1_ln_excl :: [Double -> Double] -> (Double, Double) -> IO ()
plot_fn_r1_ln_excl f = plot_fn_r1_ln_x f . plot_rng_to_x_excl 1000

-- * Csv

csv_read :: Read t => Bool -> FilePath -> IO [[t]]
csv_read hdr fn = do
  s <- readFile fn
  let t = Csv.csvTable (Csv.parseDSV False ',' s)
      p = Csv.fromCSVTable t
      d = if hdr then List.tail_err p else p
  return (map (map read) d)

csv_read_double :: Bool -> FilePath -> IO [[Double]]
csv_read_double = csv_read

to_p2 :: [a] -> (a, a)
to_p2 x =
  case x of
    [p, q] -> (p, q)
    _ -> error "tbl_p2"

to_p3 :: [a] -> (a, a, a)
to_p3 x =
  case x of
    [p, q, r] -> (p, q, r)
    _ -> error "tbl_p3"

csv_read_p2 :: Bool -> FilePath -> IO [(Double, Double)]
csv_read_p2 hdr = fmap (map to_p2) . csv_read_double hdr

csv_read_p3 :: Bool -> FilePath -> IO [(Double, Double, Double)]
csv_read_p3 hdr = fmap (map to_p3) . csv_read_double hdr

{- | Plot (x,y,z) Csv data file, with (True) or without (False) header entry.

> let fn = "/home/rohan/sw/hsc3-data/data/csv/xyz/a.csv"
> let fn = "/home/rohan/sw/hsc3-data/data/csv/trace/b.csv"
> plot_csv_p3_ln True fn
-}
plot_csv_p3_ln :: Bool -> FilePath -> IO ()
plot_csv_p3_ln hdr fn = csv_read_p3 hdr fn >>= plot_p3_ln . return

-- * Low-level

{- | Plot size is either (width,height) or ratio or Nothing.
If the ratio is + it determines image ratio, if negative it determines unit length ratio.
-}
type Plot_Size = Maybe (Either (Double, Double) Double)

def_plot_sz :: Plot_Size
def_plot_sz = Nothing -- Just (Left (1200,400)) -- Nothing

plot_size_abs :: (Double, Double) -> Plot_Size
plot_size_abs (w, h) = Just (Left (w, h))

plot_size_unit_ratio :: Plot_Size
plot_size_unit_ratio = Just (Right (-1))

-- | Plot options.
data Plot_Opt = Plot_Opt
  { plot_size :: Plot_Size
  , plot_x_range :: Maybe (Double, Double)
  , plot_y_range :: Maybe (Double, Double)
  , plot_dir :: FilePath
  , plot_name :: String
  , plot_terminal :: String
  , plot_gnuplot_opt :: [String]
  }

plot_opt_systmp :: Plot_Opt -> IO Plot_Opt
plot_opt_systmp o = do
  tmp <- getTemporaryDirectory
  return (o {plot_dir = tmp})

-- | Default options for /X11/.
plot_opt_x11 :: Plot_Opt
plot_opt_x11 = Plot_Opt Nothing Nothing Nothing "/tmp" "mkPlot" "x11" ["-p"]

-- | Default options for /Wxt/.
plot_opt_wxt :: Plot_Opt
plot_opt_wxt = plot_opt_x11 {plot_terminal = "wxt"}

-- | Default options for /Qt/.
plot_opt_qt :: Plot_Opt
plot_opt_qt = plot_opt_x11 {plot_terminal = "qt"}

-- | Default options for /Svg/.
plot_opt_svg :: Plot_Size -> Plot_Opt
plot_opt_svg sz = Plot_Opt sz Nothing Nothing "/tmp" "mkPlot" "svg" []

-- | Names for Svg terminal have character restrictions.
plot_name_enc :: Plot_Opt -> String
plot_name_enc =
  let f c = if isAlphaNum c then c else '_'
  in map f . plot_name

-- | Plot parameters, (/pre/,/cmd/,/attr/)
type Plot_Param = ([String], String, Attr)

plotDataFile :: Plot_Opt -> Int -> FilePath
plotDataFile opt k = plot_dir opt </> plot_name opt <.> show k <.> "data"

plotRcFile :: Plot_Opt -> FilePath
plotRcFile opt = plot_dir opt </> plot_name opt <.> plot_terminal opt <.> "rc"

plotOutputFile :: Plot_Opt -> FilePath
plotOutputFile opt = plot_dir opt </> plot_name opt <.> plot_terminal opt

{- | Write Gnu Plot Rc (run-control) file.

<http://gnuplot.sourceforge.net/docs_5.2/Gnuplot_5.2.pdf> pp.168-169
-}
writePlotRc :: Plot_Opt -> Plot_Param -> Int -> IO ()
writePlotRc opt (pre, cmnd, attr) n = do
  let at = map (\i -> "'" ++ plotDataFile opt i ++ "' " ++ attr (i, n)) [1 .. n]
      x_range = case plot_x_range opt of
        Just (x0, x1) -> concat ["set xrange [", show x0, ":", show x1, "]"]
        Nothing -> ""
      y_range = case plot_y_range opt of
        Just (y0, y1) -> concat ["set yrange [", show y0, ":", show y1, "]"]
        Nothing -> ""
      term = case plot_size opt of
        Just (Left (w, h)) -> printf "set terminal \"%s\" size %f,%f" (plot_terminal opt) w h
        _ -> printf "set terminal \"%s\"" (plot_terminal opt)
      size = case plot_size opt of
        Just (Left (w, h)) -> printf "set size %f,%f" w h
        Just (Right r) -> printf "set size ratio %f" r
        Nothing -> ""
      output = case plot_terminal opt of
        "svg" -> concat ["set output '", plotOutputFile opt, "'"]
        _ -> ""
      pre' =
        [ term
        , size
        , output
        , "set tics font \"cmr10, 10\""
        , x_range
        , y_range
        , "unset key"
        ]
          ++ pre
  _ <- writeFile (plotRcFile opt) (unlines pre' ++ cmnd ++ " " ++ intercalate "," at)
  return ()

-- | Write Gnu Plot data files.
writePlotData :: PNum t => Plot_Opt -> [[[t]]] -> IO ()
writePlotData opt d = do
  let f = unwords . map pshow
  zipWithM_ writeFile (map (plotDataFile opt) [1 .. length d]) (map (unlines . map f) d)

-- | Plotter given 'Plot_Opt' and 'Plot_Param'.
mk_plot_opt :: PNum t => Plot_Opt -> Plot_Param -> [[[t]]] -> IO ()
mk_plot_opt opt param dat = do
  let n = length dat
  when (any (== 0) (map length dat)) (error "mk_plot_opt: null data")
  writePlotRc opt param n
  writePlotData opt dat
  _ <- rawSystem "gnuplot" (plot_gnuplot_opt opt ++ [plotRcFile opt])
  return ()

-- | 'mk_plot_opt' with 'plot_opt_qt'.
mk_plot_qt :: (PNum t, Num t) => Plot_Param -> [[[t]]] -> IO ()
mk_plot_qt = mk_plot_opt plot_opt_qt

-- | 'mk_plot_opt' with 'plot_opt_x11'.
mk_plot_x11 :: PNum t => Plot_Param -> [[[t]]] -> IO ()
mk_plot_x11 = mk_plot_opt plot_opt_x11

-- | 'mk_plot_opt' with 'plot_opt_svg'.
mk_plot_svg :: PNum t => Plot_Size -> Plot_Dat t -> IO ()
mk_plot_svg sz (param, dat) = mk_plot_opt (plot_opt_svg sz) param dat

mk_plot_def :: (PNum t, Num t) => Plot_Dat t -> IO ()
mk_plot_def (param, dat) = do
  mk_plot_qt param dat
  mk_plot_svg plot_size_unit_ratio (param, dat)

-- | Qt and Svg.
mkPlot :: (PNum t, Num t) => Plot_Param -> [[[t]]] -> IO ()
mkPlot param dat = mk_plot_def (param, dat)
