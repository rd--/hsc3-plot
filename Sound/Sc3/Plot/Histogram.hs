-- | Histogram
module Sound.Sc3.Plot.Histogram where

import qualified Music.Theory.Math.Histogram as Histogram {- hmt-base -}

import Sound.Sc3.Plot {- hsc3-plot -}

data Histogram = Histogram [Double] [Int] deriving (Eq, Show)

-- | Calculate 'Histogram' for number of bins /n/ and sample data /x/.
bHistogram :: Int -> [Double] -> Histogram
bHistogram n x =
  let (l, h) = Histogram.bHistogram n x
  in Histogram (map fst l) h

{- | Plot 'Histogram' data.

> import qualified Sound.Sc3.Plot.Histogram as H
> plotHistogram 3 [[0,0,1,2,2,2]]
> plotHistogram 9 [[1,2,2,3,3,3,4,4,4,4]]
> plotHistogram 100 [map sin [0, 0.01 .. 2 * pi]]
-}
plotHistogram :: Int -> [[Double]] -> IO ()
plotHistogram n =
  let gen_dat x =
        let (l, h) = Histogram.bHistogram n x
        in zip (map fst l) (map fromIntegral h)
  in plot_p2_stp . map gen_dat
