-- | Fft processing & plots.
module Sound.Sc3.Plot.Fft where

import Data.Complex {- base -}

import qualified Numeric.FFT as Fft {- pure-fft -}

{-
import qualified Numeric.Fft.Vector.Invertible as N {- vector-fftw -}
import qualified Data.Vector as V {- vector -}
import qualified Sound.Sc3.Lang.Collection.Vector as V {- hsc3-lang -}
-}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Common.Math.Window as Window {- hsc3 -}

import Sound.Sc3.Plot {- hsc3-plot -}

-- * Fft

{-
-- | Real /fft/ (via vector-fftw).
rfft_vec :: [Double] -> [Double]
rfft_vec l =
    let w = Window.hamming_table (length l)
        v = V.fromList (zipWith (*) w l)
        r = N.run N.dct1 v
    in V.toList r
-}

-- | Real /fft/ of first /n/ elements of /l/ (via pure-fft).
rfft_pure_k :: Int -> [Double] -> [Double]
rfft_pure_k n l =
  let first_half = take (n `div` 2)
      w = Window.hamming_table n
      f = map abs . first_half . map realPart . Fft.fft . map (:+ 0) . zipWith (*) w
  in f l

-- | Real /fft/ of /l/ (via pure-fft).
rfft_pure :: [Double] -> [Double]
rfft_pure l = rfft_pure_k (length l) l

-- | Given fft function (ie. 'rfft_pure') and buffer index, plot Fft of buffer.
plot_buffer_fft_f :: (MonadIO m, DuplexOsc m) => ([Double] -> [Double]) -> Int -> m ()
plot_buffer_fft_f fft_f k = do
  d <- b_fetch 512 k
  liftIO (plot_p1_ln (map fft_f d))

-- | 'plot_buffer_fft_f' of 'rfft_pure'
plot_buffer_fft :: (MonadIO m, DuplexOsc m) => Int -> m ()
plot_buffer_fft = plot_buffer_fft_f rfft_pure

{- | Plot Fft of /n/ seconds of n-ary channel Ugen graph.

> plot_ugen_fft1 0.1 (sinOsc ar (sampleRate / 4) 0)
> plot_ugen_fft1 0.1 (mix (sinOsc ar (sampleRate / mce3 3 4 5) 0))
> plot_ugen_fft1 0.1 (whiteNoiseId 'α' ar)
> plot_ugen_fft1 0.1 (brownNoiseId 'α' ar)
> plot_ugen_fft1 0.1 (blip ar 1000 15)
-}
plot_ugen_fft1 :: Double -> Ugen -> IO ()
plot_ugen_fft1 = plot_ugen_opt (1000, 48000, Just 1, plot_buffer_fft)

-- | 'plot_p1_imp' of 'rfft_pure'.
plot_fft1 :: [Double] -> IO ()
plot_fft1 = plot_p1_imp . return . rfft_pure

{- | Plot of 'rfft_pure' with X-axis in fractional midi note numbers.

> let r = sineFill 8192 (map recip [55,34,1,3,2,13,5,8,21]) (replicate 9 0)
> plot_fft1_mnn 48000 r
-}
plot_fft1_mnn :: Double -> [Double] -> IO ()
plot_fft1_mnn sr r =
  let n = length r
      n2 = n `div` 2
      f = zip (map (max 0 . cps_to_midi . bin_to_freq sr n2) [0 ..])
  in plot_p2_stp [f (rfft_pure_k n r)]
