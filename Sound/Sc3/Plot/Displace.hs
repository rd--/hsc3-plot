-- | Displace
module Sound.Sc3.Plot.Displace where

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Plot {- hsc3-plot -}

{- | Variant that scales each plot lie within (0,1) and displaces them.

> plot_table_displace [[0,2..12],[0..6],[0,4..12]]
-}
plot_table_displace :: (Enum t, Fractional t, Ord t, PNum t) => [[t]] -> IO ()
plot_table_displace t =
  let rng = zip [0 ..] [1 ..]
  in plot_p1_ln (zipWith (normalise_seq (minmax (concat t))) rng t)

{- | Variant that normalises each table separately.

> plot_table_displace_nrm [[0,2..12],[0..6],[0,4..12]]
-}
plot_table_displace_nrm :: (Enum t, Fractional t, Ord t, PNum t) => [[t]] -> IO ()
plot_table_displace_nrm t =
  let rng = zip [0 ..] [1 ..]
  in plot_p1_ln (zipWith normalise_seq' rng t)

plot_buffer_displace :: (MonadIO m, DuplexOsc m) => Int -> m ()
plot_buffer_displace k = b_fetch 512 k >>= liftIO . plot_table_displace

-- | 'plot_table_displace' of 'concat' of 'mapM' of 'b_fetch'.
plot_buffer_set :: (MonadIO m, DuplexOsc m) => [Int] -> m ()
plot_buffer_set k = mapM (b_fetch 512) k >>= liftIO . plot_table_displace . concat

-- | 'plot_table_displace_nrm' of 'mapM' of 'b_fetch'.
plot_buffer_nrm :: (MonadIO m, DuplexOsc m) => [Int] -> m ()
plot_buffer_nrm k = mapM (b_fetch1 512) k >>= liftIO . plot_table_displace_nrm

plot_sf_displace :: FilePath -> IO ()
plot_sf_displace = plot_sf_ch_rng plot_table_displace Nothing Nothing

plot_sf_set_displace :: Maybe [Int] -> Maybe (Int, Int) -> [FilePath] -> IO ()
plot_sf_set_displace ix k = plot_sf_set_f plot_table_displace ix k
