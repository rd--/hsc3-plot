hsc3-plot
---------

[Haskell](http://haskell.org/)
[SuperCollider](http://audiosynth.com/)
plotting
([Gnuplot](http://www.gnuplot.info/)).

For interactive use under
[debian](http://debian.org/)
install `gnuplot-x11`, not `gnuplot`.

There is an
[entry](?t=hsc3-texts&e=2.3-plotting.md)
at
[hsc3-texts](?t=hsc3-texts).

## cli

[plot](?t=hsc3-plot&e=md/plot.md)

© [rohan drape](http://rohandrape.net/),
  2013-2024,
  [gpl](http://gnu.org/copyleft/)
